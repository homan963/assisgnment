var myApp = angular.module('myApp', ['ngCookies']);
myApp.controller('LoginController', function ($scope,$cookies, $http, $window) {
  $scope.visible = false;
  $scope.visible2 = false;

    $scope.login = function(){
      var username = $scope.account;
      var password = $scope.password ;
      var url = 'https://assisgnment2.herokuapp.com/login'
      $http.get(url).success(function(data){
        var users = data.result;
        for (var key in users){
          if(username==users[key].data.name&&password==users[key].data.pwd){
            console.log("success");
            $scope.visible = true;
            $scope.visible1 = true;
          }
        }
      })
    }

    $scope.showRegister = function(){
      $scope.visible1 = true;
      $scope.visible2 = true;

    }

    $scope.register = function(){
      url = "https://assisgnment2.herokuapp.com/register";
      $http.post(url, $scope.account)
        .success(function(data, status, headers, config) {
            console.log("success");
            $scope.account="";
            $scope.visible2 = false;
        })
        .error(function(data, status, headers, config) {
            console.log("fail");
            console.log($scope.account);
        });
    }

    $scope.getAllData = function(){
      var url = 'https://assisgnment2.herokuapp.com/'
      $http.get(url).success(function(data) {
            $scope.people = data.result;
            $scope.get = true;
        });
    }

    $scope.addData = function() {
        url = "https://assisgnment2.herokuapp.com/";
    	$http.post(url, $scope.person)
        .success(function(data, status, headers, config) {
            console.log("success");
            $scope.getAllData();
        })
        .error(function(data, status, headers, config) {
            console.log("fail");
            console.log($scope.person);
        });
    }
    $scope.delete = function(array,index){
      url = "https://assisgnment2.herokuapp.com/";
        console.log(array[index].key);
        $http.delete(url + array[index].key)
        .success(function (data) {
            console.log(data);
        });
        $scope.getAllData();

    }
    $scope.logout = function(){
      $scope.visible=false;
      $scope.visible1=false;
      $scope.account="";
      $scope.password="";
    }


})
