var http = require('http'),
    request = require('request'),
    bodyPaser = require('body-parser'),
    firebase = require('firebase'),
    express = require('express'),
    http = require('http'),
    fs = require('fs');
    
var app = express();
var config = {
    apiKey: "AIzaSyAg7PrPSk_ifKsgjDrJG0BcukcLG7GDz0I",
    authDomain: "modernweb1-b4e5e.firebaseapp.com",
    databaseURL: "https://modernweb1-b4e5e.firebaseio.com",
    projectId: "modernweb1-b4e5e",
    storageBucket: "modernweb1-b4e5e.appspot.com",
    messagingSenderId: "854408754696"
  };
firebase.initializeApp(config);
var port = process.env.PORT || 5000;

app.use(bodyPaser.json());
app.use(function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods','GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader("Access-Control-Allow-Headers","content-type");  
    res.setHeader('Access-Control-Allow-Credentials',true);
    if(req.method=="OPTIONS") res.send(200);
    else  next();
});

app.get("/login",function(req,res,next){
    var result = new Promise(function(resolve,reject){
        var records = [];
        var data;
        var ref = firebase.database().ref("users/");
        ref.once('value', function(snapshot){
            data = snapshot.val();
            
            for (var key in data){
                
                var obj = {"key":key,"data":data[key]};
                records.push(obj);
            }
        });
            
        setTimeout(function(){
            console.log(records);
            resolve({
                success:true,
                result:records
            });
        },2000);
    })
    result.then(function(value){
        res.send(value);
        res.end();
    });
})

app.post("/register",function(req,res,next){
    var bodyContent = req.body;
    var ref = firebase.database().ref("users/");
    var result = new Promise(function(resolve,reject){
        if(bodyContent.name!=undefined&&bodyContent.pwd!=undefined){
           setTimeout(function () {
              var item = ref.push({
                    name:bodyContent.name,
                    pwd:bodyContent.pwd
                });
                resolve({
                    success:true,
                    message:"Added"
                })
            }, 2000)
        }
        
    })
    result.then(function(value){
        res.send(value);
        res.end();
    });
});

app.get("/", function (req, res, next) {
    var result = new Promise(function(resolve,reject){
        var records = [];
        var data;
        var ref = firebase.database().ref("record/");
        ref.once('value', function(snapshot){
            data = snapshot.val();
            
            for (var key in data){
                
                var obj = {"key":key,"data":data[key]};
                records.push(obj);
            }
        });
            
        setTimeout(function(){
            console.log(records);
            resolve({
                success:true,
                result:records
            });
        },2000);
    })
    result.then(function(value){
        res.send(value);
        res.end();
    });
});

app.get("/:id",function (req,res,next){
    var result = new Promise(function(resolve, reject){
        var body = req.params;
        var data ;
        var ref = firebase.database().ref("record/"+body.id);
        ref.once('value').then(function(snapshot) {
          data = snapshot;
        });
        
        setTimeout(function(){
            if(data.val()!=null){
                resolve({
                    success:true,
                    result:data
                })
            }else{
                resolve({
                    success:false,
                    message:"No record"
                })
            }
        },2000);
    })
    
    result.then(function(value){
        res.send(value);
        res.end();
    });
});

app.post("/",function (req,res,next){
    var bodyContent = req.body;
    var result = new Promise(function(resolve,reject){
        var ref = firebase.database().ref("record/");
        if(bodyContent.origin!=undefined && bodyContent.destination!=undefined && bodyContent.mode!=undefined){
            var json = callApi(bodyContent.origin, bodyContent.destination, bodyContent.mode);
        }else{
            resolve({
                success:false,
                message:"Miss information"
            })
        }
        setTimeout(function () {
            console.log(json.distance);
          var item = ref.push({
                origin:bodyContent.origin,
                destination:bodyContent.destination,
                mode:bodyContent.mode,
                distance:json.distance,
                duration:json.duration
            });
            resolve({
                success:true,
                message:"Added"
            })
        }, 2000)
    })
    result.then(function(value){
        //send the json to body
        res.send(value);
        res.end();
    });

});



app.put("/:id",function (req,res,next){
    var bodyContent = req.body;
    var result = new Promise(function(resolve,reject){
        var ref = firebase.database().ref("record/"+req.params.id);
        if(bodyContent.origin!=undefined && bodyContent.destination!=undefined && bodyContent.mode!=undefined){
            var json = callApi(bodyContent.origin, bodyContent.destination, bodyContent.mode);
        }else{
            resolve({
                success:false,
                message:"Miss information"
            })
        }
        setTimeout(function () {
            console.log(bodyContent.destination);
            console.log(json);
            console.log(json.distance);
            var postData = {origin:bodyContent.origin,
                    destination:bodyContent.destination,
                    mode:bodyContent.mode,
                    distance:json.distance,
                    duration:json.duration}
            var updates = postData;
            ref.update(updates);
            
            resolve({
                success:true,
                message:"Updated"
            })
        }, 2000)
    })
    result.then(function(value){
        res.send(value);
        res.end();
    });
});

app.delete("/:id",function (req,res,next){
    
    var result = new Promise(function(resolve,reject){
        var ref = firebase.database().ref("record/");
        if(req.params.id != null){
            ref.child(req.params.id).remove();
            resolve({
                success:true,
                message:"Delete successful"
            })
        }else{
            resolve({
                success:false,
                message:"No record"
            })
        }
    })
    result.then(function(value){
       res.send(value);
       res.end();
    });
    
});

function callApi(origin,destination,mode){
    var url = "https://maps.googleapis.com/maps/api/directions/json?origin="+origin+"&destination="+destination+"&mode="+mode+"&key=AIzaSyBglyftJ7GfPW6xy9vUrF3-H_zHs02E3CI";
    var json = {};
    request(url, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var data = JSON.parse(body);
            
            if(data.routes != null){
                json.distance = data.routes[0].legs[0].distance.text;
                json.duration = data.routes[0].legs[0].duration.text;
            }else{
                json = data;
            }
        }
    });
    return json;
}

http.createServer(app).listen(port, function(){
  console.log('Express server listening on port ' + port);
});
